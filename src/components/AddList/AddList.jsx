import React, {useEffect, useState} from "react";
import axios from 'axios';

import List from "../List/List";
import Budge from "../Budge/Budge";
import './AddList.scss'

import closeSvg from '../../assets/img/close.svg'

const AddList = ({colors, onAddList}) => {
    const [visiblePopup, setVisiblePopup] = useState(false);
    const [selectedColor, selectColor] = useState(3);
    const [isLoading, setLoading] = useState(false);
    const [inputValue, setInputValue] = useState('');

    useEffect(() => {
        if (Array.isArray(colors)) {
            selectColor(colors[0].id)
        }
    }, [colors]);

    const onClose = () => {
        setVisiblePopup(false);
        setInputValue('');
        selectColor(colors[0].id);
    };

    const addList = () => {
        if (!inputValue) {
            alert("Введіть назву списка");
            return;
        }

        setLoading(true);

        axios.post('http://localhost:3001/lists',
            {
                name: inputValue,
                colorId: selectedColor
            })
            .then(({data}) => {
                const color = colors.filter(c => c.id === selectedColor)[0];
                const listObj = {...data, color, tasks: []};

                onAddList(listObj);
                onClose();
            })
            .catch(() => {
                alert('Помилка при добавлені списка')
            })
            .finally(() => {
                setLoading(false);
            })
    };

    return (
        <div className="add-list">
            <List
                onClick={() => setVisiblePopup(true)}
                items={[
                    {
                        className: "list__add-button",
                        icon: <svg width="12" height="12" viewBox="0 0 16 16" fill="none"
                                   xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 1V15" stroke="black" strokeWidth="2" strokeLinecap="round"
                                  strokeLinejoin="round"/>
                            <path d="M1 8H15" stroke="black" strokeWidth="2" strokeLinecap="round"
                                  strokeLinejoin="round"/>
                        </svg>,
                        name: "Добавити список"
                    }
                ]}
            />
            {visiblePopup && <div className="add-list__popup">
                <img
                    onClick={onClose}
                    src={closeSvg}
                    alt={"close button"}
                    className="add-list__popup-close-btn"
                ></img>
                <input className="field" type="text" placeholder="Назва списка"
                       value={inputValue} onChange={e => setInputValue(e.target.value)}/>
                <div className="add-list__popup-colors">
                    {
                        colors.map(color =>
                            <Budge key={color.id}
                                   onClick={() => selectColor(color.id)}
                                   color={color.name}
                                   className={selectedColor === color.id && 'active'}
                            />)
                    }
                </div>
                <button
                    onClick={addList}
                    className="button"
                >
                    {isLoading ? 'Добавляється...' : 'Добавити'}
                </button>
            </div>}
        </div>
    )
};

export default AddList;
