import React, {useState} from "react";
import axios from 'axios';

import './AddTasksForm.scss';

import addSvg from "../../../assets/img/add.svg";

const AddTasksForm = ({list, onAddTask}) => {

    const [visibleFrom, setFormVisible] = useState(false);
    const [inputValue, setInputValue] = useState('');
    const [isLoading, setIsLoading] = useState('');

    const toggleFormVisible = () => {
        setFormVisible(!visibleFrom);
        setInputValue('');
    };

    const addTask = () => {
        const obj = {
            listId: list.id,
            text: inputValue,
            completed: false
        };

        setIsLoading(true);

        axios.post('http://localhost:3001/tasks/', obj).then(({data}) => {

            onAddTask(list.id, obj);
            toggleFormVisible();
        })
            .catch(() => {
                alert('Помилка при добавлені задачі')
            })
            .finally(() => {
                setIsLoading(false);
            })
    };

    return (
        <div className="tasks__form">
            {!visibleFrom ?
                (<div
                        onClick={toggleFormVisible}
                        className="tasks__form-new"
                    >
                        <img src={addSvg} alt="add icon"/>
                        <span>Нова задача</span>
                    </div>
                ) : (
                    <div className="tasks__form-block">
                        <input
                            value={inputValue}
                            className="field"
                            type="text"
                            placeholder="Текст задачі"
                            onChange={e => setInputValue(e.target.value)}
                        />
                        <button
                            disabled={isLoading}
                            onClick={addTask}
                            className="button"
                        >
                            {isLoading ? 'Добавляється...' : 'Добавити задачу'}
                        </button>
                        <button
                            onClick={toggleFormVisible}
                            className="button button--grey"
                        >
                            Скасувати
                        </button>
                    </div>
                )
            }
        </div>
    )
};
export default AddTasksForm;
