import React from "react";
import './Budge.scss'
import classNames from 'classnames'

const Budge = ({color, onClick, className}) => {

    return (
        <i
            onClick={onClick}
            className={classNames('badge', {[`badge--${color}`]: color}, className)}
        ></i>
    )
};

export default Budge;
